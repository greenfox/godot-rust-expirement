#!/bin/sh

main()
{
    . $HOME/.cargo/env
    
    build x86_64-pc-windows-gnu
    build x86_64-unknown-linux-gnu

    cp target/x86_64-unknown-linux-gnu/release/libsdf.so ../test-project/libsdf/
    cp target/x86_64-pc-windows-gnu/release/sdf.dll ../test-project/libsdf/

}

build()
{
    TARGET=$1
    settup ${TARGET}
    cargo build --target ${TARGET}
    cargo build --target ${TARGET} --release      
}

settup()
{
    TARGET=$1
    rustup target install ${TARGET}
}

REALWORKINGDIR=$(dirname $0)
cd $REALWORKINGDIR
main $@
